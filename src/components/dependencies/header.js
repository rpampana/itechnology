function Header() {
  var list = ["Technology","Service","Portfolio","About Us", "Career","Blog"];
  return (
    <header id="topHeader">
      <ul id="topInfo">
        <li>+974 98765432</li>
        <li>info@itecnology.com</li>
      </ul>

      <nav>
        <span className="logo">iTechnology</span>
        <div className="menu-btn-3" onclick="menuBtnFunction(this)">
          <span></span>
        </div>
        <div className="mainMenu">
          {list.map((option) => {
            return (
              <a href="">
                <span>option</span>
              </a>
            );
          })}
          <a href="">Work With Us</a>
        </div>
      </nav>
    </header>
  );
}
export default Header;