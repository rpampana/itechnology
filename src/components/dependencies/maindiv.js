
function Maindiv() {
	return (
		<>
			<Intro />
			<Delivery />
		</>
	)
}

function Intro() {
	return (
		<>
			<section id="intro">
				<div id="intro-info">
					<div>
						<h1>Full Service Mobile App Development Company</h1>
						<div id="intro-tag-btn">
							<span>Over 100M app downloads across 1500+ projects.</span>
							<a href="" className="brand-btn">Let's Talk</a>
						</div>
					</div>
				</div>

				<div id="development-img">
					<img src="https://www.dropbox.com/s/7hwnjccu15vt90e/mobileDevlopment.svg?raw=1" alt="Mobile App Development" title="Mobile App Development" />
				</div>
			</section></>
	)
}
function Delivery() {
	return (
		<>
			<section id="delivery">
				<h1 className="sec-heading">Delivering Experience Since 2009</h1>
				<div className="col-5 delivery-img">
					<img src="https://www.dropbox.com/s/ipx91osglyczpdt/delivery_experience.svg?raw=1" alt="Productivity Delivering Experience" title="Delivering Experience Since 2009" />
				</div>
				<div className="col-7">
					<h2>Accelerating your business growth with Digital Experiences</h2>
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br /><br /> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</p>
					<div className="btn-footer">
						<a href="" className="brand-btn">Contact Us</a>
					</div>
				</div>
			</section>

		</>
	)
}
export default Maindiv;