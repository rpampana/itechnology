function Footer() {
  var col1 = [
    "Interprise App Development",
    "Android App Development",
    "ios App Development",
  ];
  var col2 = ["Healthcare", "Sports", "ECommerce", "Construction", "Club"];
  var col3 = ["Reviews", "Terms & Condition", "Disclaimer", "Site Map"];
  return (
    <>
      <footer>
        <div>
          <span className="logo">iTechnology</span>
        </div>

        <div className="row">
          <div className="col-3">
            <span className="footer-cat">Solution</span>
            <ul className="footer-cat-links">
              {col1.map((col) => {
                return (
                  <li>
                    <a href="">
                      <span>{col}</span>
                    </a>
                  </li>
                );
              })}
            </ul>
        
          </div>
          <div className="col-3">
            <span className="footer-cat">Industries</span>
            <ul className="footer-cat-links">
              {col2.map((col) => {
                return (
                  <li>
                    <a href="">
                      <span>{col}</span>
                    </a>
                  </li>
                );
              })}
            </ul>
         
          </div>
          <div className="col-3">
            <span className="footer-cat">Quick links</span>
            <ul className="footer-cat-links">
              {col3.map((col) => {
                return (
                  <li>
                    <a href="">
                      <span>{col}</span>
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
          <div class="col-3" id="newsletter">
                <span class="footer-cat">Stay Connected</span>
                <form id="subscribe">
                    <input type="email" id="subscriber-email" placeholder="Enter Email Address"/>
                    <input type="submit" value="Subscribe" id="btn-scribe"/>
                </form>
                
               

                <div id="address">
                    <span class="footer-cat">Office Location</span>
                    <ul>
                        <li>
                            <i class="far fa-building"></i>
                            <div>Los Angeles<br/>
                            Office 9B, Sky High Tower, New A Ring Road, Los Angeles</div>
                        </li>
                        <li>
                            <i class="fas fa-gopuram"></i>
                            <div>Delhi<br/>
                            Office 150B, Behind Sana Gate Char Bhuja Tower, Station Road, Delhi</div>
                        </li>
                    </ul>
                </div>
                

          
        </div>
        </div>
        <div id="copyright">&copy; All Rights Reserved 2019-2020</div>
        <div id="owner">
          <span>
            Designed by
            <a href="https://www.codingtuting.com">CodingTuting.Com</a>
          </span>
        </div>
        <a href="#topHeader" id="gotop">
          Top
        </a>
      </footer>
    </>
  );
}
export default Footer