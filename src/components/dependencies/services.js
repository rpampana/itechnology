function Services() {
  var shortcuts = [
    { icon: "fas fa-laptop", name: "Stratagy and Consultant" },
    { icon: "fas fa-users", name: "User Experience Design" },
    { icon: "fas fa-mobile-alt", name: "Mobile App Development" },
    { icon: "fab fa-chrome", name: "Web App Development" },
    { icon: "fas fa-ribbon", name: "Quality Analysis and Testing" },
    { icon: "fas fa-ticket-alt", name: "Application Management & Support" },
  ];
  return (
    <>
      <section id="services">
        <h1 className="sec-heading">Our Services</h1>
        <ul>
          {shortcuts.map((data) => {
            return <Service icon={data.icon} name={data.name} />;
          })}
        </ul>

        <div id="service-footer">
          <a href="" className="brand-btn">
            View All Service
          </a>
        </div>
      </section>
    </>
  );
}
function Service(props) {
  return (
    <>
      <li>
        <div>
          <a href="">
            <i className={props.icon}></i>
            <span>{props.name}</span>
          </a>
        </div>
      </li>
    </>
  );
}
export default Services;
